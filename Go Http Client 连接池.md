## 使用常见场景
在后端编码，也会涉及频繁调用第三方sdk进行验证等，比如 验证、授权、支付等。场景这是Sdk Api使用Http居多，Http 客户端在发完一个请求后并不会立即关闭 Tcp 连接，它会继续等待一段时间，如果该连接上又有新的 Http 请求，就复用这个连接。Tcp 主动关闭的一方会有 TIME_WAIT 状态，该状态会占用端口资源，如果服务器在短时间内发起大量的外部 Http 请求，将会积压很多 TIME_WAIT 连接. 

通常需要使用连接池来复用一个或者多个连接解决问题。
## Go Http Client 连接池
下面看一个完整实现例子

```go
client := &http.Client{
		Timeout: time.Second * 10,
		Transport: &http.Transport{
			ForceAttemptHTTP2:     true,
			MaxIdleConns:          3,
			IdleConnTimeout:       90 * time.Second,
			TLSHandshakeTimeout:   10 * time.Second,
			ExpectContinueTimeout: 1 * time.Second,
		},
	}
	req,err := http.NewRequest("POST","http://localhost:8090",nil)
	if err != nil {
		log.Fatal(err)
	}
	res,err := client.Do(req)
	if err != nil {
		log.Fatal(err)
	}
	defer res.Body.Close()
	content,err := ioutil.ReadAll(res.Body)
	if err != nil {
		log.Fatal(err)
	} 
	fmt.Println(string(content))

```
## 设置 Transport 
```go
type Transport struct {

//MaxIdleConns控制最大空闲数(keep-alive)
//连接到所有主机。零意味着没有限制。
	MaxIdleConns int

//MaxIdleConnsPerHost，如果非0，控制最大空闲
//(keep-alive)连接保持每个主机。如果为零,
//DefaultMaxIdleConnsPerHost已被使用。
	MaxIdleConnsPerHost int

//MaxConnsPerHost可选限制总数
//每个主机的连接，包括拨号中的连接，
//激活和空闲状态。如果违反限制，拨号将被阻塞。
//
//0表示没有限制。
	MaxConnsPerHost int

//IdleConnTimeout是最大空闲时间
//(keep-alive)连接在关闭前将保持空闲状态本身。
//0表示没有限制。
	IdleConnTimeout time.Duration

//ResponseHeaderTimeout，如果非0，指定数量
//等待服务器响应头的时间
//写请求(包括它的主体，如果有的话)。这
//time不包括读取响应体的时间。
	ResponseHeaderTimeout time.Duration
}
```
 
 通过设置 http.Transport.MaxIdleConns 参数可限制最大的连接数量，从而实现连接复用，不会大量创建新的连接、端口。